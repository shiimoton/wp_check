const Log4js = require("log4js");
const setting = require('../../setting/log-config.json');
const option ={
	"appenders": {
		"console": {
			"type": "console",
			"level": "all"
		},
		"error": {
			"type": "dateFile",
			"filename": "log/error.log",
			"pattern": "-yyyy-MM-dd",
			'maxLogSize': 100, 
			'backups': 3
		}
	},
	"categories": {
		"default": {
			"appenders": [
				"console"
			],
			"level": "info"
		},
		"debug": {
			"appenders": [
				"console"
			],
			"level": "all"
		},
		"error": {
			"appenders": [
				"console",
				"error"
			],
			"level": "warn"
		},
	}
}

option.categories.debug.level = setting.level;
Log4js.configure(option);
const systemLogger = Log4js.getLogger('default');
const debugLogger = Log4js.getLogger("debug");
const errorLogger = Log4js.getLogger("error");


const log = {};
log.trace = function(errorStr,fileName){
	debugLogger.trace('['+fileName+']'+errorStr)
}
log.debug = function(errorStr,fileName){
	debugLogger.debug('['+fileName+']'+errorStr)
}
log.info = function(errorStr,fileName){
	systemLogger.info('['+fileName+']'+errorStr)
}
log.error = function(errorStr,fileName){
	errorLogger.error('['+fileName+']'+errorStr)
}
log.system = function(systemMassage){
	console.log(systemMassage);
}
log.console = function(log){
	if(setting.console){
		console.log(log);
	}
}
module.exports = log;
