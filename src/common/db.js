const mysql = require('mysql2/promise');

const db = {};
db.setting = {
	host: 'localhost',
	user: 'root',
	password: '',
	database: 'wp_check'
};
db.con = {};
db.connect = async function(){
	try {
		db.con = await mysql.createConnection(db.setting);
	}catch(e){
		console.log(e);
		throw (e);
	}

}
db.select = async function(sql,data){
	try {
		await db.connect();
		const [ret, fields]  = await db.con.query(sql,data);

		return ret;
	}catch(e){
		console.log(e);
	}finally{
		await db.con.end();
	}
}

module.exports = db;