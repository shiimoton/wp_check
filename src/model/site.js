const db = require('../common/db.js');


const site = {};

site.getSiteDate = async function() {
	let sql = 'SELECT * FROM site';
	let d = [];
	const ret  = await db.select(sql,d);
	return ret;
}

module.exports = site;