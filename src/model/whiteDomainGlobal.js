const db = require('../common/db.js');


const whiteDomainGlobal = {};

whiteDomainGlobal.getAll = async function() {
	let sql = ''; 
	sql = sql +  'SELECT * ';
	sql = sql + ' FROM white_domain_global';
	sql = sql + ' WHERE delete_date IS NULL';
	let d = [];
	const ret  = await db.select(sql,d);
	const list = [];
	ret.forEach(row =>{
		list.push(row.domain);
	});
	return list;
}

module.exports = whiteDomainGlobal;
