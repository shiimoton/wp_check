const urlHelper = require('url');
const path = require('path');
const log = require('../common/log.js');
const siteData = require('../data/siteData.js');
const pupService = require('./puppeteerService.js');
const siteService = require('./siteService.js');
const siteErrorService =require('../service/siteErrorService.js');
const fileName = 'service/pageService';
const pageService = {}

pageService.notAddExt = ['jpg','jpeg','png','gif'];

pageService.getAddPageList = async function(pageLinkList,nowUrl){
	log.info('run .getAddPageList',fileName);
	pageLinkList.forEach(url =>{
		if(url.indexOf(siteData.siteSetting.url) < 0){
			//'urlの中にsiteSettingと違うドメインが入っているので、
			//'許されているドメインかチェックする。;
			let urlAry = urlHelper.parse(url);
			
			if(
				urlAry.host != '' 
				&& urlAry.host != siteData.siteSetting.option.host 
				&& siteService.isWhiteDomein(urlAry.host) === false
			){	
				siteErrorService.otherDomain(url,nowUrl);
			}
		}else{
			if(pageService._isPushLinkListUrl(url,nowUrl)){
				log.trace(url,'siteService.pageListに追加する。');
				row = {};
				row.url = url;
				row.hasUrlPages = [];
				row.hasUrlPages.push(nowUrl);
				siteData.pageList.push(row);
			}
		}
	});
	return;
}


pageService._isPushLinkListUrl = function(url,nowUrl){
	// log.info('run _isPushLinkListUrl',fileName);
	addBool = true;
	let ext = path.extname(url).split('.').join('');
	//ページ以外の物jpgやpngは追加しない
	if(pageService.notAddExt.indexOf(ext) >= 0){
		addBool = false;
	}else{
		// TODO ここのループがかっこ悪いから、http://lifelog.main.jp/wordpress/?p=2557のfilter関数で駒氏にする。
		for(let i = 0; i< siteData.pageList.length; i++){
			if(siteData.pageList[i].url == url){
				
				if(siteData.pageList[i].hasUrlPages === undefined){
					siteData.pageList[i].hasUrlPages = [];
				}
				if(siteData.pageList[i].hasUrlPages.indexOf(url) < 0 && siteData.pageList[i].hasUrlPages.indexOf(nowUrl) < 0){
					siteData.pageList[i].hasUrlPages.push(nowUrl);
				}
				addBool = false;
				break;
			}
		}
	}
	return addBool;
}

module.exports = pageService;
