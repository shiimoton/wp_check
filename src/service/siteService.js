const urlHelper = require('url');
const log = require('../common/log.js');

const siteData = require('../data/siteData.js');
const whiteDomainData = require('../data/whiteDomainData.js');

const fileName = 'service/siteService';

const siteService = {};

siteService.setSiteSetting = function(site){
	log.info('run setSiteSetting',fileName);
	site.option = urlHelper.parse(site.url);
	siteData.siteSetting = site;
	//ページリストの1個目にトップページのURLを入れる
	row = {};
	row.title = '';
	row.url = site.url;
	siteData.pageList.push(row);
	
	//TODO　サイト毎のOKドメインリストを取得する。
	siteWhiteDomain = [];
	siteData.whiteDomain = siteWhiteDomain.concat(whiteDomainData.global);
}

siteService.isWhiteDomein = function(targDomain){
	log.debug('run isWhiteDomein',fileName);
	if(siteData.whiteDomain.indexOf(targDomain) >= 0){
		// log.debug(siteData.whiteDomain.indexOf(targDomain),fileName);
		return true;
	}
	return false;
}



module.exports = siteService;
