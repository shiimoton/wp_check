var moment = require('moment');
const siteData = require('../data/siteData.js');


const siteErrorService = {};

siteErrorService.errorMsg = {
	'404':'URLのページが有りません',
	'otherDomain':'サイト外へのリンクがあります'
}
/**
 * ドメインエラー
 * @type {[type]}
 */
siteErrorService.otherDomain = function(url,nowUrl,errorStr){
	msg = siteErrorService.errorMsg.otherDomain;
	let type = 'warn';
	siteData.errors.push({
		'time':moment().format('YYYY-MM-DD hh:mm:ss'),
		'url':url,
		'type':type,
		'code':'hasOtherDomain',
		'message':msg,
		'hasUrlPages':nowUrl
	});
}

/**
 * 400系と500系エラー
 * @param  {[type]} pageData  [description]
 * @param  {[type]} errorCode [description]
 * @param  {[type]} errorStr  [description]
 * @return {[type]}           [description]
 */
siteErrorService.response = function(pageData,errorCode,errorStr){
	let type = 'warn';
	if(errorCode){

	}
	msg =siteErrorService.errorMsg[errorCode];
	siteData.errors.push({
		'time':moment().format('YYYY-MM-DD hh:mm:ss'),
		'url':pageData.url,
		'type':type,
		'code':errorCode,
		'message':msg,
		'hasUrlPages':pageData.hasUrlPages
	});
}

module.exports = siteErrorService;
