const puppeteer = require('puppeteer');
const log = require('../common/log.js');


const fileName = 'service/puppeteerService';

const pupService = {};
pupService.browser = null;
pupService.page = {};
pupService.linkList = [];
pupService.siteSetting = {};
pupService.start = async function(){
	log.info('run pupService.start',fileName);
	if(pupService.browser === null){
		log.trace('make pupService.browser',fileName);
		pupService.browser = await puppeteer.launch();
	}
	log.trace('make newPage',fileName);
	pupService.page = await pupService.browser.newPage();
	return;
}


pupService._getPageLinks = async function(){
	log.info('run _getPageLinks',fileName);
	const pageLinkList = await pupService.page.evaluate(() => {
		const list = [];
		const nodeList = document.querySelectorAll("a");
		nodeList.forEach(_node => {
			ary = [];
			ary = _node.href.split('#');
			url = ary[0];
			ary = url.split('?');
			url = ary[0];
			list.push(url);
		})
		return list;
	});
	return pageLinkList;
}



module.exports = pupService
