const log = require('../common/log.js');
const siteData = require('../data/siteData.js');
const pupService =require('../service/puppeteerService.js');
const siteService =require('../service/siteService.js');
const pageService =require('../service/pageService.js');
const siteErrorService =require('../service/siteErrorService.js');
const urlHelper = require('url');

const fileName = 'controller/checkRedirect';


const checkRedirect = {};

checkRedirect.run = async function(site){
	log.debug('run checkRedirect.run',fileName);
	log.system('サイト内のページURLを全て取得しています 上限：'+site.max_page+'ページ');

	siteService.setSiteSetting(site);

	await pupService.start();
	let count = 0;
	do{
		await checkRedirect.check(count);
		count++;
	}while (siteData.pageList.length > count);
	// }while (count < 3 );


	await pupService.page.close();
	console.log(siteData.pageList);
	console.log(siteData.pageList.length);
	console.log(siteData.errors);
	return;
}



checkRedirect.check = async function(count){
	log.info('run .check【count:'+count+'/'+siteData.pageList.length+'】',fileName);
	log.info('checkurl【'+siteData.pageList[count].url+'】',fileName);
	log.info('run check',fileName);
	//ページを移動
	let response = await pupService.page.goto(siteData.pageList[count].url);
	let responseStatus = response.status();
	if(responseStatus == 404){
		siteErrorService.response(siteData.pageList[count],'404');
		return;
	}


	log.trace('リダイレクト数をカウント');
	let redirectCount = response.request().redirectChain().length;

	// TODO リダイレクトを正しく検出できているかチェックが必要。
	// TODO リダイレクト先が正しい場合はエラーを出さないという事は可能か？
	if(redirectCount > 0){
		console.log(redirectCount);
		//TODO サイトエラーを表示する。
	}	
	//TODO ページ内で読み込まれているファイルのレスポンプステータスをチェックして
	//TODO 200系以外の物をエラーで表示する。
	
	//表示ページのタイトルを取得する。
	siteData.pageList[count].title = await pupService.page.title();

	//ページ内のリンクを全て取得する。
	if(siteData.pageList.length <= siteData.siteSetting.max_page){
		log.trace('取得したpage数がsiteのmax_pageにたっしていない。');
		const pageLinkList = await pupService._getPageLinks();
		await pageService.getAddPageList(pageLinkList,siteData.pageList[count].url);
	}
	return;
}



module.exports = checkRedirect
