const site = require('./src/model/site.js');
const checkRedirect = require('./src/controller/checkRedirect.js');
const whiteDomainService = require('./src/service/whiteDomainService.js');

const main = async function(){
	
	
	await whiteDomainService.getSetGrobal();
	let siteList = await site.getSiteDate();
	for(let i =0; i<siteList.length;i++){
		await checkRedirect.run(siteList[i]);
	}
	// TODO エラー内容をファイルに記載する。
	console.log('end');
}

main();
